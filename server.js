const express = require('express');
const mongoose = require('mongoose');
const config = require('config');
const cors = require('cors');

//get all Routes
const items = require('./routes/api/items');
const users = require('./routes/api/users');
const auth = require('./routes/api/auth');
const path = require('path');

const app = express(); //Intiliaze express
app.use(express.json());
app.use(cors())

//Get Configurations
const db = config.get("mongoURI");

mongoose.connect(db , {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true
}).then(console.log('Mongo DB connected successfully'))
    .catch(err => console.log(err));

//Use Routes
app.use('/api/items', items);
app.use('/api/users', users);
app.use('/api/auth', auth);

if (process.env.NODE_ENV === 'production') {
    app.use(express.static('client/build'));

    app.get('*', (req, res) => {
        res.sendFile(path.resolve(__dirname, 'client/build', 'index.html'))
    })
}

//Check for the environment port
const port = process.env.PORT || 5000;
app.listen(port, () => console.log(`Server using ${port}`));