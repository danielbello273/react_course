const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//Create Schema

const ItemSchema = new Schema({
name: {
    type: String,
    require: true
},
surname: {
    type: String,
    require: true
},
birthday: {
    type: String,
    require: true
},
address: {
    type: String,
    require: true
},
socialSecNr: {
    type: String,
    require: true
},
age: {
    type: String,
    require: true
},
date: {
    type: Date,
    default: Date.now
  }
});

module.exports = Item = mongoose.model('item', ItemSchema);