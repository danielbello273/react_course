import React from 'react';
import { Formik, Form, FormikProps } from 'formik';
import { FormGroup, FormControl, Input, InputLabel, Button } from '@material-ui/core';
//import { login } from '../../redux/actions/authActions';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
//import { clearErrors } from "../../redux/actions/errorActions";
import { register } from "../../redux/actions/authActions";
import axios from "axios";

const Register = props => {
    const initialValues = { name: '', email: '', password: '' };

    const handleSubmit = values => {
        console.log(values)

        props.register(values);
    };

    return (
        <div>
            <div>
                <h1>Register HERE</h1>
                <Formik
                    initialValues={initialValues}
                    onSubmit={handleSubmit}
                    validate={values => {
                        let errors = {};
                        if (!values.email) {
                            errors.email = 'Required';
                        } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)) {
                            errors.email = 'Invalid email address';
                        }
                        return errors;
                    }}
                >
                    {(props) => (
                        <Form>
                            <FormGroup>
                                <FormControl>
                                    <InputLabel id="nameLabel">Name</InputLabel>
                                    <Input
                                        name="name"
                                        type="text"
                                        onChange={props.handleChange}
                                        onBlur={props.handleBlur}
                                        value={props.values.name}
                                    />
                                </FormControl>
                                <FormControl>
                                    <InputLabel id="emailLabel">Email</InputLabel>
                                    <Input
                                        name="email"
                                        type="text"
                                        onChange={props.handleChange}
                                        onBlur={props.handleBlur}
                                        value={props.values.email}
                                    />
                                </FormControl>
                                <span style={{ color: 'red' }}>{props.errors.email ? props.errors.email : ''}</span>
                                <FormControl>
                                    <InputLabel id="passwordLabel">Password</InputLabel>
                                    <Input
                                        name="password"
                                        type="password"
                                        onChange={props.handleChange}
                                        onBlur={props.handleBlur}
                                        value={props.values.password}
                                    />
                                </FormControl>
                                <Button
                                    type="submit"
                                    variant={'outlined'}
                                    color={'primary'}
                                    disabled={props.isSubmitting && !props.isValid}
                                >
                                    Submit
                                </Button>
                            </FormGroup>
                        </Form>
                    )}
                </Formik>
            </div>
        </div>
    );
};

Register.propTypes = {
    loggedIn: PropTypes.bool.isRequired,
    login: PropTypes.func.isRequired,
};

Register.defaultProps = {
    loggedIn: false,
    login: () => {},
};

const mapStateToProps = state => ({
    isAuthenticated: state.auth.isAuthenticated,
});

const mapDispatchToProps = {
    register
};

export default connect(mapStateToProps, mapDispatchToProps)(Register);
