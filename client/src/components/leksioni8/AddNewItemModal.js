import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTheme } from '@material-ui/core/styles';
import { FormControl, Input, InputLabel } from "@material-ui/core";


const AddNewItem = (props) => {
    const [open, setOpen] = React.useState(false);
    const theme = useTheme();
    const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));
    const [itemData, setItemData] = React.useState({});

    const handleClickOpen = () => {
        setOpen(true);
        setItemData(props.itemData)
    };

    const handleClose = () => {
        setOpen(false);
        setItemData({});
    };

    const handleInputChange = e => {

        const value = e.target.value;
        const name = e.target.name;

        const changedObject = {
            ...itemData,
            [name]: value
        }

        setItemData(changedObject);
    }

    const handleSubmit = () => {
        props.addItem(itemData);
        props.refreshTable();
        handleClose();
    }

    return (
        <div>
            <Button variant="outlined" color="primary" onClick={handleClickOpen}>
                Add New Item
            </Button>
            <Dialog
                fullScreen={fullScreen}
                open={open}
                onClose={handleClose}
                aria-labelledby="responsive-dialog-title"
            >
                <DialogTitle id="responsive-dialog-title">{"Use Google's location service?"}</DialogTitle>
                <DialogContent>

                    <FormControl style={{ marginBottom: "2rem", marginRight: "2rem" }}>
                        <InputLabel
                            id="name"
                        >
                            Name
                        </InputLabel>
                        <Input
                            name={"name"}
                            onChange={handleInputChange}
                        />
                    </FormControl>
                    <FormControl>
                        <InputLabel
                            id="surname"
                        >
                            Surname
                        </InputLabel>

                        <Input
                            name={"surname"}
                            onChange={handleInputChange}
                            required={true}
                        />

                        <Button
                            variant={"outlined"}
                            color={"primary"}
                            style={{ marginTop: "2rem" }}
                            onClick={handleSubmit}
                        >
                            Add Item
                        </Button>
                    </FormControl>
                </DialogContent>
                <DialogActions>
                    <Button autoFocus onClick={handleClose} color="primary">
                        Disagree
                    </Button>
                    <Button onClick={handleClose} color="primary" autoFocus>
                        Agree
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
    );
}

export default AddNewItem;