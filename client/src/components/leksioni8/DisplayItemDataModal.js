import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { FormControl, Input, InputLabel } from "@material-ui/core";
import moment from "moment";
import FormGroup from "@material-ui/core/FormGroup";
import { connect } from "react-redux";
import {getItemByID} from "../../redux/actions/itemActions";


class ResponsiveDialogClass extends React.Component{

    state = {
        open: false,
        name: '',
        surname: '',
        itemData: {
            id: 0,
            name: '',
            surname: '',
            date: Date.now()
        }
    }

    handleInputChange = e => {
        const value = e.target.value;
        const name = e.target.name;

        const { itemData } = this.state;

        const newItemData = {
            ...itemData,
            [name]: value
        }

        this.setState({itemData: newItemData});
    }

    toggle = () => {
        if (!this.state.open) {
            this.props.getItemByID(this.props.itemId);
        }
        this.setState({open: !this.state.open})
    }

    render() {

        const { open, itemData } = this.state;

        return (
            <div>
                <Button variant="outlined" color="primary" onClick={this.toggle}>
                    Display Data
                </Button>
                <Dialog
                    open={open}
                    onClose={this.toggle}
                    aria-labelledby="responsive-dialog-title"
                >
                    <DialogTitle id="responsive-dialog-title">{"Use Google's location service?"}</DialogTitle>
                    <DialogContent>
                        <FormGroup>
                            <FormControl style={{ marginBottom: "2rem" }}>
                                <InputLabel
                                    id="name"
                                >
                                    Name
                                </InputLabel>
                                <Input
                                    name={"name"}
                                    onChange={this.handleInputChange}
                                    value={itemData.name}
                                />
                            </FormControl>

                            <FormControl style={{ marginBottom: "2rem" }}>
                                <InputLabel
                                    id="surname"
                                >
                                    Surname
                                </InputLabel>
                                <Input
                                    name={"surname"}
                                    onChange={this.handleInputChange}
                                    value={itemData.surname}
                                />
                            </FormControl>
                        </FormGroup>

                        <DialogContentText>
                            {moment(itemData.date).format('YYYY-MM-DD')}
                        </DialogContentText>
                    </DialogContent>
                    <DialogActions>
                        <Button autoFocus onClick={this.toggle} color="primary">
                            Disagree
                        </Button>
                        <Button onClick={this.toggle} color="primary" autoFocus>
                            Agree
                        </Button>
                    </DialogActions>
                </Dialog>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    item: state.item
})

const mapDispatchToProps = {
    getItemByID
}

export default connect(mapStateToProps, mapDispatchToProps)(ResponsiveDialogClass);