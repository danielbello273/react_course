import React from 'react';
import axios from 'axios';

class ShowUpdateDetails extends React.Component {

    state = {
        itemData: {
            _id: 0,
            name: '',
            surname: ''
        }
    }

    componentDidMount() {
        console.log(this.props.itemId);
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (prevProps.itemId !== this.props.itemId) {
            axios.get('/api/items/' + this.props.itemId)
                .then(res => this.setState({itemData: res.data}))
                .catch(err => console.log(err));
        }
    }

    render() {

        const { itemData } = this.state;

        return (
            <div>
               <h1>{itemData.name}</h1>
               <h1>{itemData.surname}</h1>
            </div>
        );
    }
}

export default ShowUpdateDetails;