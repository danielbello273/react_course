import React, { Component } from 'react';
import axios from 'axios';
import MUIDataTable from "mui-datatables";
import DisplayItemDataModal from "../detyra10/DisplayItemDataModal";
import AddNewItemModal from "../detyra10/AddNewItemModal";
import {connect} from "react-redux";
import {Button} from "@material-ui/core";
import moment from "moment";
import { getItems, addItem } from "../../redux/actions/itemActions";

class Item extends Component {
    state = {
        Employee: {
            name: "Test",
            surname: "test",
            birthday: '',
            address: '',
            socialSecNr: '',
            age: '',
            Date
        },
        items: [],
        itemId: null,
    }
    componentDidMount() {
        // this._getData();
        // this.props.getItems();
        console.log(this.props.items);
        this.setState({items: this.props.items});
    }
    _getData = () => {
        axios.get('/api/items')
            .then(res => {
                console.log(res.data);
                this.setState({items: res.data})
            });
    }
    render() {
        const {items} = this.props;
        const columns = [
            "Name",
            "Date",
            "Name",
            "Surname",
            "Birthday",
            "Address",
            "SocialSecNr",
            "Age",
            "Date",
            "Actions",
            "Class Action"
        ];
        const options = {
            filterType: "dropdown",
            responsive: "vertical",
            isRowSelectable: function(dataIndex) {
                return true;
            }
        };
        const data = items.map( item => {
            return [
                item._id,
                item.name,
                item.surname,
                item.birthday,
                item.address,
                item.socialSecNr,
                item.age,
                moment(item.date).format('YYYY-MM-DD hh:mm:ss'),
                <DisplayItemDataModal
                    key={item._id}
                    itemData={item}
                />
            ]
        } )
        return (
            <React.Fragment>
                <AddNewItemModal
                    refreshTable={this._getData}
                    addItem={this.props.addItem}
                />
                <Button
                    onClick={this.props.getItems}
                    className="btn btn-success"
                >Click to get Items</Button>
              {
                    data.length ? (<MUIDataTable
                        title={"List of Items"}
                        data={data}
                        columns={columns}
                        options={options}
                    />) : null
                }
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    items: state.items
});

// const mapDispatchToProps = () => ({
//     getItems
// })

export default connect( mapStateToProps, {getItems, addItem} )(Item);