import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { FormControl, Input, InputLabel } from "@material-ui/core";
import moment from "moment";


class ResponsiveDialogClass extends React.Component{

    state = {
        open: false,
        name: '',
        surname: '',
        itemData: {
            name: "",
            surname: "",
            birthday: '',
            address: '',
            socialSecNr: '',
            age: '',
            date: Date.now()
        }
    }

    componentDidMount() {
        this.setState({itemData: this.props.itemData})
    }

    handleInputChange = e => {
        const value = e.target.value;
        const name = e.target.name;

        const { itemData } = this.state;

        const newItemData = {
            ...itemData,
            [name]: value
        }

        this.setState({itemData: newItemData});
    }

    toggle = () => {
        this.setState({open: !this.state.open})
    }

    render() {

        const { open, itemData } = this.state;

        return (
            <div>
                <Button variant="outlined" color="primary" onClick={this.toggle}>
                    Display Data 111
                </Button>
                <Dialog
                    open={open}
                    onClose={this.toggle}
                    aria-labelledby="responsive-dialog-title"
                >
                    <DialogTitle id="responsive-dialog-title">{"Update item data"}</DialogTitle>
                    <DialogContent>

                        <FormControl style={{ marginBottom: "2rem" }}>
                            <InputLabel
                                id="name"
                            >
                                Name
                            </InputLabel>
                            <Input
                                name={"name"}
                                onChange={this.handleInputChange}
                                value={itemData.name}
                            />
                        </FormControl>

                        <FormControl style={{ marginBottom: "2rem" }}>
                            <InputLabel
                                id="surname"
                            >
                                Surname
                            </InputLabel>
                            <Input
                                name={"surname"}
                                onChange={this.handleInputChange}
                                value={itemData.surname}
                            />
                        </FormControl>

                        <FormControl style={{ marginBottom: "2rem" }}>
                            <InputLabel
                                id="birthday"
                            >
                                Birthday
                            </InputLabel>
                            <Input
                                name={"birthday"}
                                onChange={this.handleInputChange}
                                value={itemData.birthday}
                            />
                        </FormControl>

                        <FormControl style={{ marginBottom: "2rem" }}>
                            <InputLabel
                                id="address"
                            >
                                Address
                            </InputLabel>
                            <Input
                                name={"address"}
                                onChange={this.handleInputChange}
                                value={itemData.address}
                            />
                        </FormControl>

                        <FormControl style={{ marginBottom: "2rem" }}>
                            <InputLabel
                                id="socialSecNr"
                            >
                                Social SecNr
                            </InputLabel>
                            <Input
                                name={"socialSecNr"}
                                onChange={this.handleInputChange}
                                value={itemData.socialSecNr}
                            />
                        </FormControl>

                        <FormControl style={{ marginBottom: "2rem" }}>
                            <InputLabel
                                id="5"
                            >
                                Age
                            </InputLabel>
                            <Input
                                name={"age"}
                                onChange={this.handleInputChange}
                                value={itemData.age}
                            />
                        </FormControl>

                        <DialogContentText>
                            {moment(itemData.date).format('YYYY-MM-DD')}
                        </DialogContentText>
                    </DialogContent>
                    <DialogActions>
                        <Button autoFocus onClick={this.toggle} color="primary">
                            Cancel
                        </Button>
                        <Button onClick={this.toggle} color="primary" autoFocus>
                            Save
                        </Button>
                    </DialogActions>
                </Dialog>
            </div>
        );
    }
}

export default ResponsiveDialogClass;