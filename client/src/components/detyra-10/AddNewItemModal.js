import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import { FormControl, Input, InputLabel } from "@material-ui/core";

const AddNewItem = (props) => {
    const [open, setOpen] = React.useState(false);
    const [itemData, setItemData] = React.useState({});

    const handleClickOpen = () => {
        setOpen(true);
        setItemData(props.itemData)
    };

    const handleClose = () => {
        setOpen(false);
        setItemData({});
    };

    const handleInputChange = e => {

        const value = e.target.value;
        const name = e.target.name;

        const changedObject = {
            ...itemData,
            [name]: value
        }

        setItemData(changedObject);
    }

    const handleSubmit = () => {
        props.addItem(itemData);
        props.refreshTable();
        handleClose();
    }

    return (
        <div>
            <Button variant="outlined" color="primary" onClick={handleClickOpen}>
                Add New Item
            </Button>
            <Dialog
                open={open}
                onClose={handleClose}
                aria-labelledby="responsive-dialog-title"
            >
                <DialogTitle id="responsive-dialog-title">{"Add new item"}</DialogTitle>
                <DialogContent>

                    <FormControl style={{ marginBottom: "2rem", marginRight: "2rem" }}>
                        <InputLabel
                            id="name"
                        >
                            Name
                        </InputLabel>
                        <Input
                            name={"name"}
                            onChange={handleInputChange}
                        />
                    </FormControl>
                    <FormControl>
                        <InputLabel
                            id="surname"
                        >
                            Surname
                        </InputLabel>

                        <Input
                            name={"surname"}
                            onChange={handleInputChange}
                            required={true}
                        />
                    </FormControl>

                    <FormControl>
                        <InputLabel
                            id="birthday"
                        >
                            Birthday
                        </InputLabel>

                        <Input
                            name={"birthday"}
                            onChange={handleInputChange}
                            required={true}
                        />
                    </FormControl>

                    <FormControl>
                        <InputLabel
                            id="address"
                        >
                            Address
                        </InputLabel>

                        <Input
                            name={"address"}
                            onChange={handleInputChange}
                            required={true}
                        />
                    </FormControl>

                    <FormControl>
                        <InputLabel
                            id="socialSecNr"
                        >
                            Social SecNr
                        </InputLabel>

                        <Input
                            name={"socialSecNr"}
                            onChange={handleInputChange}
                            required={true}
                        />
                    </FormControl>

                    <FormControl>
                        <InputLabel
                            id="age"
                        >
                            Age
                        </InputLabel>

                        <Input
                            name={"age"}
                            onChange={handleInputChange}
                            required={true}
                        />
                    </FormControl>
                </DialogContent>
                <DialogActions>
                    <Button autoFocus onClick={handleClose} color="primary">
                        Disagree
                    </Button>
                    <Button onClick={handleSubmit} color="primary" autoFocus>
                        Add Item
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
    );
}


export default AddNewItem;