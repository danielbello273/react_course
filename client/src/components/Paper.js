import React, { Component } from "react";
import PaperChild from "./Paperchild";
import '../css/Paper.css';

export default class SimplePaper extends Component {
    state = {
        person: [
            {
                firstName: "Daniel"
            },
            {
                lastName: "Bello"
            },
            {
                profession: "Developer"
            }
        ],
    };

    render() {

        const { person } = this.state;
        return (
            <div className="parent">
                <PaperChild person={person} />
            </div>
        );
    }
}
