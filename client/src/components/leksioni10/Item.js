import React, { Component } from 'react';
import MUIDataTable from "mui-datatables";
import DisplayItemDataModal from "../leksioni8/DisplayItemDataModal";
import AddNewItemModal from "../leksioni8/AddNewItemModal";
import {connect} from "react-redux";
import {Button} from "@material-ui/core";
import moment from "moment";
import { getItems, deleteItem, addItem, getItemByID } from "../../redux/actions/itemActions";
import Spinner from "../leksioni11/Spinner";
import DisplayDataModal from "../detyra-12/DisplayEmployeeDataFunction";

class Item extends Component {
    constructor(props) {
        super(props);
    }

    state = {
        items: [],
    }
    componentDidMount() {
        this._getData();
        console.log(this.state.items);
    }
    _getData = () => {
        this.props.getItems();
        console.log(this.state.items);
    }
    render() {
        const {items} = this.props;

        const columns = [
            "Name",
            "Date",
            "Actions",
            "Delete Action",
            "Get Item by ID"
        ];
        const options = {
            filterType: "dropdown",
            responsive: "vertical",
            isRowSelectable: function(dataIndex) {
                return true;
            }
        };
        const data = items.map( item => {
            return [
                item.name,
                moment(item.date).format('YYYY-MM-DD hh:mm:ss'),
                <DisplayDataModal
                    key={item._id}
                    itemData={item}
                    itemId={item._id}
                />,
                <Button variant={"outlined"}
                        onClick={() => this.props.deleteItem(item._id)}
                >Delete</Button>,
                <Button onClick={() => this.props.getItemByID(item._id)}>
                    Edit item
                </Button>
            ]
        } )
        return (
            <React.Fragment>
                <AddNewItemModal
                    refreshTable={this._getData}
                    addItem={this.props.addItem}
                />
              {
                    data.length ? (<MUIDataTable
                        title={"List of Items"}
                        data={data}
                        columns={columns}
                        options={options}
                    />) : <Spinner />
                }
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    items: state.item.items
})

const mapDispatchToProps = {
    getItems,
    deleteItem,
    addItem,
    getItemByID
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
    )(Item);