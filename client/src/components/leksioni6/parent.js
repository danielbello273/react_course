import React, {Component} from 'react';
import Players from "../leksioni6/Players";
import AddItem from "../leksioni6/AddPlayer";
import './player.css';

class ParentList extends Component {

    state = {
        persons: [
            {id: 1, name: 'Paolo Maldini', age: 50, team: "AC Milan"},
            {id: 2, name: 'Javier Zanetti', age: 52, team: "FC Inter"},
            {id: 3, name: 'Alex Del Piero', age: 46, team: "FC Juventus"}
        ],
        playerId: '',
        playerName: '',
        playerAge: '',
        playerTeam: '',

    }

    deletePersonHandler = (index) => {
        const persons = [...this.state.persons];
        persons.splice(index, 1);
        this.setState({persons: persons});
    }

    nameChangeHandler = (event, id) => {
        const personIndex = this.state.persons.findIndex(
            p => {
                return p.id === id;
            }
        )

        const person = {
            ...this.state.persons[personIndex]
        }

        person.name = event.target.value;

        const persons = [...this.state.persons];
        persons[personIndex] = person;

        this.setState({persons: persons});
    }

    ageChangeHandler = (event, id) => {
        const personIndex = this.state.persons.findIndex(
            p => {
                return p.id === id;
            }
        )

        const person = {
            ...this.state.persons[personIndex]
        }

        person.age = event.target.value;

        const persons = [...this.state.persons];
        persons[personIndex] = person;

        this.setState({persons: persons});
    }


    addPlayerAge = (event) => {
        let playerAge = event.target.value;
        this.setState({playerAge : playerAge});
    }

    addPlayerID = (event) => {
        let playerID = event.target.id;
        console.log(playerID);
        this.setState({playerId : playerID});
    }

    addPlayerName = (event) => {
        let playerNameAfter = event.target.value;
        this.setState({playerName : playerNameAfter});
    }

    addPlayerTeam = (event) => {
        let playerTeamAfter = event.target.value;
        this.setState({playerTeam : playerTeamAfter});
    }

    addNewElement = (event) => {
        let newPlayer = {
            id: this.state.playerId,
            name: this.state.playerName,
            age: this.state.playerAge,
            team: this.state.playerTeam
        }

        newPlayer.id = parseInt(newPlayer.id);
        newPlayer.id = parseInt(newPlayer.age);

        const persons = [...this.state.persons];

        persons.push(newPlayer)
        this.setState({persons: persons});
    }

    render() {

        let persons = (
            this.state.persons.map( (person, index) => {
                return <Players
                    name={person.name}
                    age={person.age}
                    key={person.id}
                    showEdit={person.showEdit}
                    deleteEvent={ () => this.deletePersonHandler(index) }
                    nameChangeHandler={(event) => this.nameChangeHandler(event, person.id)}
                    ageChangeHandler={(event) => this.ageChangeHandler(event, person.id)}
                    toggleEdit = { () => this.toggleEdit()}
                />
            } )
        );

        return (
            <div className="App" style={{marginTop: '2rem'}}>
                {persons}
                <AddItem addNewElement={(event) => this.addNewElement(event)}
                         addPlayerAge = {(event) => this.addPlayerAge(event)}
                         addPlayerName = {(event) => this.addPlayerName(event)}
                         addPlayerID = {(event) => this.addPlayerID(event)}
                         addPlayerTeam = {(event) => this.addPlayerTeam(event)}
                />
            </div>
        )
    }
}

export default ParentList;
