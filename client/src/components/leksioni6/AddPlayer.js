import React from 'react';
import PropTypes from 'prop-types';

class AddItem extends React.Component {
    state = {
        player: [
            {id: '', name: '', age: '', team: ""}
        ]
    }

    render() {
        return (
            <div className="col-xs-12 mt-2"
                style={{ boxShadow: '1px 1px 1px 1px #ccc', marginBottom: '2rem', padding: '2rem', backgroundColor: '#00ffff' }}
            >
                <div className="container">
                    <h3 style={{textTransform: "uppercase"}}>Add new football player</h3>
                    <div className="col-xs-12 mt-2">
                        <label htmlFor="id">ID:</label>
                        <input type="number" aria-label="id" name={this.props.id} id={this.props.id} onChange={this.props.addPlayerID} value={this.props.id}/>
                    </div>

                    <div className="col-xs-12 mt-2">
                        <label htmlFor="name">Name:</label>
                        <input type="text" aria-label="Name" id="name" onChange={this.props.addPlayerName} value={this.props.name}/>
                    </div>

                    <div className="col-xs-12 mt-2">
                        <label htmlFor="age">Age:</label>
                        <input type="number" aria-label="age" id="age" onChange={this.props.addPlayerAge} value={this.props.age}/>
                    </div>

                    <div className="col-xs-12 mt-2">
                        <label htmlFor="team">Team:</label>
                        <input type="text" aria-label="team" id="team" onChange={this.props.addPlayerTeam} value={this.props.team}/>
                    </div>

                    <button type="button" className="btn btn-dark" onClick={this.props.addNewElement}>Add Element</button>

                </div>
            </div>
        );
    }
}

// DisplayPersons.propTypes = {
//     name: PropTypes.string.isRequired,
//     age: PropTypes.number.isRequired,
//     deleteEvent: PropTypes.func.isRequired,
//     toggleEdit: PropTypes.func.isRequired,
//     nameChangeHandler: PropTypes.func.isRequired,
//     ageChangeHandler: PropTypes.func.isRequired,
// }
//
// DisplayPersons.defaultProps = {
//     name: 'Test',
//     age: '20',
//     deleteEvent: () => {},
//     toggleEdit: () => {},
//     nameChangeHandler: () => {},
//     ageChangeHandler: () => {},
// }


export default AddItem;