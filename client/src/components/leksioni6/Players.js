import React from 'react';
import PropTypes from 'prop-types';

class Players extends React.Component {
    render() {
        return (
            <div className="col-md-4 col-sm-12 player-container"
            >
                <p>Name : {this.props.name}</p>
                <p>Age : {this.props.age}</p>
                <input type="text" onChange={this.props.nameChangeHandler} value={this.props.name}/>
                <input type="number" onChange={this.props.ageChangeHandler} value={this.props.age}/>
                <button className="btn btn-danger" onClick={this.props.deleteEvent}> Delete Person </button>
            </div>
        );
    }
}

Players.propTypes = {
    name: PropTypes.string.isRequired,
    age: PropTypes.number.isRequired,
    deleteEvent: PropTypes.func.isRequired,
    toggleEdit: PropTypes.func.isRequired,
    nameChangeHandler: PropTypes.func.isRequired,
    ageChangeHandler: PropTypes.func.isRequired,
}

Players.defaultProps = {
    name: 'Test',
    age: '20',
    deleteEvent: () => {},
    toggleEdit: () => {},
    nameChangeHandler: () => {},
    ageChangeHandler: () => {},
}


export default Players;