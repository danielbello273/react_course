import React, { Component } from 'react';
import axios from 'axios';
import MUIDataTable from "mui-datatables";
import DisplayItemDataModal from "../detyra-12/DisplayItemDataModal";
import AddNewItemModal from "../detyra-12/AddNewItemModal";
import {connect} from "react-redux";
import {Button} from "@material-ui/core";
import {getItems, addItem, getItemByID, deleteItem} from "../../redux/actions/itemActions";
import DisplayEmployeeData from "./DisplayEmployeeDataFunction";

class Employees extends Component {
    state = {
        Employee: {
            name: "",
            surname: "",
            birthday: '',
            address: '',
            socialSecNr: '',
            age: '',
            Date
        },
        items: [],
        itemId: null,
    }
    componentDidMount() {
        this.props.getItems();
    }

    _getData = () => {
        this.props.getItems();
    }

    render() {
        const {items} = this.props;
        const columns = [
            "ID",
            "Name",
            "Surname",
            "Edit",
            "Delete"
        ];
        const options = {
            filterType: "dropdown",
            responsive: "vertical",
            isRowSelectable: function(dataIndex) {
                return true;
            }
        };
        const data = items.map( item => {
            return [
                item._id,
                item.name,
                item.surname,
                <DisplayEmployeeData
                    key={item._id}
                    itemId={item._id}
                />,
                <Button variant={"outlined"}
                        onClick={() => this.props.deleteItem(item._id)}
                >Delete</Button>,
            ]
        } )
        return (
            <React.Fragment>
                <AddNewItemModal
                    refreshTable={this._getData}
                    addItem={this.props.addItem}
                />
              {
                    data.length ? (<MUIDataTable
                        title={"List of Employees"}
                        data={data}
                        columns={columns}
                        options={options}
                    />) : null
                }
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    items: state.item.items
})

const mapDispatchToProps = {
    getItems,
    deleteItem,
    addItem,
    getItemByID
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Employees);