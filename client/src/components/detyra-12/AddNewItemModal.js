import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import { FormControl, Input, InputLabel } from "@material-ui/core";
import { Formik, Form, Field } from "formik";
import FormGroup from "@material-ui/core/FormGroup";
import * as Yup from 'yup';
import '../../App.css';

const AddNewItem = (props) => {
    const [open, setOpen] = React.useState(false);
    const [itemData, setItemData] = React.useState({});

    const handleClickOpen = () => {
        setOpen(true);
        setItemData(props.itemData)
    };

    const handleClose = () => {
        setOpen(false);
        setItemData({});
    };

    const handleSubmit = values => {
        console.log(values);
        const newItem = {
            name: values.name,
            surname: values.surname,
            birthday: values.birthday,
            address: values.address,
            socialSecNr: values.socialSecNr,
            age: values.age
        }
        props.addItem(newItem);
        props.refreshTable();
        handleClose();
    }

    const employeeSchema = Yup.object().shape({
        name: Yup.string()
            .min(2, 'Too Short!')
            .max(20, 'Too Long!')
            .required('Name is Required'),
        surname: Yup.string()
            .min(2, 'Too Short!')
            .max(20, 'Too Long!')
            .required('Surname is Required'),
        birthday: Yup.date().required("Date Required").nullable(),
    });

    function validateSocialNr(value) {
        let error;
        if (!value) {
            error = 'Required';
        } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)) {
            error = 'Invalid social number';
        }
        return error;
    }

    return (
        <div>
            <Button variant="outlined" color="primary" onClick={handleClickOpen}>
                Add New Item
            </Button>
            <Dialog
                open={open}
                onClose={handleClose}
                aria-labelledby="responsive-dialog-title"
            >
                <DialogTitle id="responsive-dialog-title">{"Add new item"}</DialogTitle>
                <DialogContent>

                    <Formik initialValues={{ name: "",
                        surname: "",
                        birthday: "",
                        address: "",
                        socialSecNr: "",
                        age: 0
                    }}
                            onSubmit={handleSubmit}
                            validationSchema={employeeSchema}
                    >
                        { ({ errors, touched, isValidating}) => (
                            <Form className={"form-container"}>
                                <FormGroup>
                                    <FormControl style={{ marginBottom: "2rem", marginRight: "2rem" }}>
                                        <InputLabel
                                            id="name"
                                        >
                                            Name
                                        </InputLabel>
                                        <Field name="name" />
                                        {errors.name && touched.name ? (
                                            <div className={"error"}>{errors.name}</div>
                                        ) : null}
                                    </FormControl>

                                    <FormControl style={{ marginBottom: "2rem", marginRight: "2rem" }}>
                                        <InputLabel
                                            id="surname"
                                        >
                                            Surname
                                        </InputLabel>
                                        <Field name="surname" />
                                        {errors.surname && touched.surname ? (
                                            <div className={"error"}>{errors.surname}</div>
                                        ) : null}
                                    </FormControl>

                                    <FormControl style={{ marginBottom: "2rem", marginRight: "2rem" }}>
                                        <InputLabel
                                            id="birthday"
                                        >
                                            Birthday
                                        </InputLabel>

                                        <Field name="birthday" type={"date"}/>
                                        {errors.birthday && touched.birthday ? (
                                            <div className={"error"}>{errors.birthday}</div>
                                        ) : null}
                                    </FormControl>

                                    <FormControl style={{ marginBottom: "2rem", marginRight: "2rem" }}>
                                        <InputLabel
                                            id="address"
                                        >
                                            Address
                                        </InputLabel>

                                        <Field
                                            name={"address"}
                                        />
                                    </FormControl>

                                    <FormControl style={{ marginBottom: "2rem", marginRight: "2rem" }}>
                                        <InputLabel
                                            id="socialSecNr"
                                        >
                                            Social SecNr
                                        </InputLabel>

                                        <Field
                                            name={"socialSecNr"}
                                            validate={validateSocialNr}
                                        />
                                        {errors.socialSecNr && touched.socialSecNr && <div className={"error"}>{errors.socialSecNr}</div>}
                                    </FormControl>

                                    <FormControl style={{ marginBottom: "2rem", marginRight: "2rem" }}>
                                        <InputLabel
                                            id="age"
                                        >
                                            Age
                                        </InputLabel>

                                        <Field
                                            name={"age"}
                                            type={"number"}
                                        />
                                    </FormControl>
                                    <Button color="primary" autoFocus type="submit">
                                        Add Item
                                    </Button>
                                </FormGroup>
                            </Form>
                        ) }
                    </Formik>
                </DialogContent>
                <DialogActions>
                    <Button autoFocus onClick={handleClose} color="primary">
                        Disagree
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
    );
}


export default AddNewItem;