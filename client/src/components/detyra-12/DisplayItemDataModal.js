import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { FormControl, Input, InputLabel } from "@material-ui/core";
import moment from "moment";
import {getItemByID} from "../../redux/actions/itemActions";
import {connect} from "react-redux";
import Employee from "../leksioni12/Employee";


class ResponsiveDialogClass extends React.Component{

    state = {
        open: false,
        name: '',
        surname: '',
        itemData: {
            name: "",
            surname: "",
            birthday: '',
            address: '',
            socialSecNr: '',
            age: '',
            date: Date.now()
        },
        Employee: '',
    }

    componentDidMount() {
        this.setState({itemData: this.props.itemData});
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        console.log(this.props.itemData);
        if (prevProps.itemData !== this.props.itemData && this.state.open) {
            this.props.getItemByID(this.props.itemData._id);
            console.log(this.props.itemData);
        }
    }

    handleInputChange = e => {
        const value = e.target.value;
        const name = e.target.name;

        const { itemData } = this.state;

        const newItemData = {
            ...itemData,
            [name]: value
        }

        this.setState({itemData: newItemData});
    }

    toggle = () => {
        this.setState({open: !this.state.open});
    }

    render() {

        const { open, itemData } = this.state;

        return (
            <div>
                <Button variant="outlined" color="primary" onClick={this.toggle}>
                    Edit Employee
                </Button>
                <Dialog
                    open={open}
                    onClose={this.toggle}
                    aria-labelledby="responsive-dialog-title"
                >
                    <DialogTitle id="responsive-dialog-title">{"Update item data"}</DialogTitle>
                    <DialogContent>

                        <FormControl style={{ marginBottom: "2rem" }}>
                            <InputLabel
                                id="name"
                            >
                                Name
                            </InputLabel>
                            <Input
                                name={"name"}
                                onChange={this.handleInputChange}
                                defaultValue={itemData.name}
                            />
                        </FormControl>

                        <FormControl style={{ marginBottom: "2rem" }}>
                            <InputLabel
                                id="surname"
                            >
                                Surname
                            </InputLabel>
                            <Input
                                name={"surname"}
                                onChange={this.handleInputChange}
                                defaultValue={itemData.surname}
                            />
                        </FormControl>

                        <FormControl style={{ marginBottom: "2rem" }}>
                            <InputLabel
                                id="birthday"
                            >
                                Birthday
                            </InputLabel>
                            <Input
                                name={"birthday"}
                                onChange={this.handleInputChange}
                                defaultValue={itemData.birthday}
                            />
                        </FormControl>

                        <FormControl style={{ marginBottom: "2rem" }}>
                            <InputLabel
                                id="address"
                            >
                                Address
                            </InputLabel>
                            <Input
                                name={"address"}
                                onChange={this.handleInputChange}
                                defaultValue={itemData.address}
                            />
                        </FormControl>

                        <FormControl style={{ marginBottom: "2rem" }}>
                            <InputLabel
                                id="socialSecNr"
                            >
                                Social SecNr
                            </InputLabel>
                            <Input
                                name={"socialSecNr"}
                                onChange={this.handleInputChange}
                                defaultValue={itemData.socialSecNr}
                            />
                        </FormControl>

                        <FormControl style={{ marginBottom: "2rem" }}>
                            <InputLabel
                                id="5"
                            >
                                Age
                            </InputLabel>
                            <Input
                                name={"age"}
                                onChange={this.handleInputChange}
                                defaultValue={itemData.age}
                            />
                        </FormControl>

                        <DialogContentText>
                            {moment(itemData.date).format('YYYY-MM-DD')}
                        </DialogContentText>
                    </DialogContent>
                    <DialogActions>
                        <Button autoFocus onClick={this.toggle} color="primary">
                            Cancel
                        </Button>
                        <Button onClick={this.toggle} color="primary" autoFocus>
                            Save
                        </Button>
                    </DialogActions>
                </Dialog>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    item: state.item
});

const mapDispatchToProps = {
    getItemByID
}

export default connect(mapStateToProps, mapDispatchToProps)(ResponsiveDialogClass);