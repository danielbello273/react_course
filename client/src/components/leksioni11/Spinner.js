import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        '& > * + *': {
            marginLeft: theme.spacing(2),
        },
    },
    paolo: {
        padding: theme.spacing(3)
    }
}));

export default function Spinner() {
    const classes = useStyles();

    return (
        <div className={classes.paolo}>
            <CircularProgress/>
        </div>
    );
}