import React from 'react';
import axios from 'axios';

const ShowUpdateDetailsFunction = (props) => {

    const [itemData, setItemData] = React.useState({name: '', surname: ''})

    const myRef = React.useRef();

    React.useEffect(() => {
        axios.get('/api/items/' + props.itemId)
            .then(res => {
                setItemData(res.data)
                console.log(res.data)
            })

    }, [props.itemId])

    return (
        <div>
            <h1>{itemData.name}</h1>
            <h1>{itemData.surname}</h1>
        </div>
    );
}

export default ShowUpdateDetailsFunction;