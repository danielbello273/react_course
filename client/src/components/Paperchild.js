import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';

const useStyles = makeStyles((theme) => ({
    parentDiv: {
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'center',
        alignItems: 'center',
        alignContent: 'center',
        '& > *': {
            margin: theme.spacing(1),
            width: theme.spacing(16),
            height: theme.spacing(16),
        },
    },
}));

export default function PaperChild (props) {

    const classes = useStyles();

    return (
        <div className={classes.parentDiv}>
            <h3 className={"title"}>Test Person</h3>
            <Paper elevation={0} style={{backgroundColor: "#ccc", color: "#fff", fontWeight: "900"}}>
                { props.person.map( person =>
                    <h2>{person.firstName}</h2>
                ) }
            </Paper>
            <Paper className={"last-name"}>
                { props.person.map( person =>
                    <h2>{person.lastName}</h2>
                ) }
            </Paper>
            <Paper elevation={3} className={"profession"}>
                { props.person.map( person =>
                    <h2>{person.profession}</h2>
                ) }
            </Paper>
        </div>
    );

}

PaperChild.propTypes = {
    person: PropTypes.array
}

PaperChild.defaultProps = {
    person: [{ lastName: 'Default Lastname' }]
}
