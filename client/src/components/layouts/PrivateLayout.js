import React from "react";
import { Redirect, withRouter, Route, Switch } from "react-router-dom";
import { compose } from "recompose";
import Employee from "../leksioni12/Employee";
import AppNavBar from "../navigations/AppNavBar";
import { connect } from "react-redux";
import Item from "../leksioni10/Item";


const PrivateLayout = (props) => {

    return (
        <div>
            <AppNavBar>
                { (props.isAuthenticated || props.isLoading) ?
                    <Switch>
                        <Route exact path="/" component={Item} />
                        <Route exact path="/employees" component={Employee} />
                    </Switch>
                :
                    <Redirect to="/app/login" />
                }
            </AppNavBar>
        </div>
    );
}

const mapStateToProps = state => ({
    isAuthenticated: state.auth.isAuthenticated,
    isLoading: state.auth.isLoading
})

export default compose(
    withRouter,
    connect(mapStateToProps, null)
)(PrivateLayout);