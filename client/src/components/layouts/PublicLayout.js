import React from "react";
import { Redirect, withRouter, Route, Switch } from "react-router-dom";
import { compose } from "recompose";
import Login from "../authentication/Login";
import AppNavBar from "../navigations/AppNavBar";
import { connect } from "react-redux";

const PublicLayout = (props) => {

    if (!props.isAuthenticated) {
        return (
            <div>
                <AppNavBar>
                    <Switch>
                        <Route exact path="/app/login" component={Login} />
                    </Switch>
                </AppNavBar>
            </div>
        );
    } else {
        return <Redirect to="/" />;
    }
}

const mapStateToProps = state => ({
    isAuthenticated: state.auth.isAuthenticated
})

export default compose(
    withRouter,
    connect(mapStateToProps, null)
)(PublicLayout);