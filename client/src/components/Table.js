import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { FormControl, Input, InputLabel } from "@material-ui/core";
import moment from "moment";
import axios from "axios";


export default function ResponsiveDialog(props) {
    const [open, setOpen] = React.useState(false);
    const [itemData, setItemData] = React.useState({});

    const handleClickOpen = () => {
        setOpen(true);
        setItemData(props.itemData);
    };

    const handleClose = () => {
        console.log(itemData);
        setOpen(false);
        setItemData({});
    };

    const editItem = () => {
        axios.put('/api/items/' + itemData._id, itemData)
            .then(res => {
                setItemData(res.data)
                handleClose();
                props.refresh();
            })
            .catch(err => console.log(err));

        handleClose();
    }

    const handleInputChange = e => {

        const value = e.target.value;
        const name = e.target.name;

        const changedObject = {
            ...itemData,
            [name]: value
        }

        setItemData(changedObject);
    }

    return (
        <div>
            <Button variant="outlined" color="primary" onClick={handleClickOpen}>
                Display Data
            </Button>
            <Dialog
                open={open}
                onClose={handleClose}
                aria-labelledby="responsive-dialog-title"
            >
                <DialogTitle id="responsive-dialog-title">{"Use Google's location service?"}</DialogTitle>
                <DialogContent>

                    <FormControl style={{ marginBottom: "2rem" }}>
                        <InputLabel
                            id="name"
                        >
                            Name
                        </InputLabel>
                        <Input
                            name={"name"}
                            onChange={handleInputChange}
                            defaultValue={itemData.name}
                        />
                    </FormControl>

                    <FormControl style={{ marginBottom: "2rem" }}>
                        <InputLabel
                            id="surname"
                        >
                            Name
                        </InputLabel>
                        <Input
                            name={"surname"}
                            onChange={handleInputChange}
                            defaultValue={itemData.surname}
                        />
                    </FormControl>

                    <DialogContentText>
                        {moment(itemData.date).calendar()}
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button autoFocus onClick={handleClose} color="primary">
                        Cancel
                    </Button>
                    <Button onClick={editItem} color="primary" autoFocus>
                        Save
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
    );
}
