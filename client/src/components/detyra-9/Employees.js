import React, { Component } from 'react';
import axios from 'axios';
//Navigation
import MUIDataTable from "mui-datatables";
import {Button} from "@material-ui/core";
import AddNewEmployee from "./AddNewEmployee";
import EditEmployee from "./ShowElements";
import Alert from "./Alert";
import './style.css';

class Employees extends Component {

    constructor(props) {
        super(props);
    }

    state = {
        Employee: {
            name: "Test",
            surname: "test",
            birthday: '',
            address: '',
            socialSecNr: '',
            age: '',
            Date
        },
        items: [],
        itemId: null,
        alert: 'Default message'
    }
    componentDidMount() {
        this._refreshItems();
    }

    showAlert = alert => {
        this.setState({alert});
        const element = document.querySelector(".alert-success");
        element.classList.add("show");

        setTimeout(function () {
            element.classList.remove("show");
        }, 5000);
    }

    _refreshItems = () => {
        axios.get('/api/items')
            .then(res => {
                this.setState({items: res.data});
            });
    }

    handleMyData = itemId => {
        this.setState({itemId});
    }


    deleteItem = itemId => {
        axios.delete('/api/items/' + itemId)
            .then(res => this._refreshItems())
            .catch(err => console.log(err));
        this.setState({alert: "Item has been deleted"});
    }

    render() {
        const { items, alert }  = this.state;

        const columns = [
            "ID",
            "Name",
            "Surname",
            "Birthday",
            "Address",
            "Social SecNr",
            "Date",
            "Age",
            "Edit Item",
            "Delete"
        ];

        const options = {
            filterType: "dropdown",
            responsive: "vertical"
        };

        const data = items.map( item => {
            return [
                item._id,
                item.name,
                item.surname,
                item.birthday,
                item.address,
                item.socialSecNr,
                item.age,
                item.date,
                <EditEmployee
                    itemData={item}
                    refresh={this._refreshItems}
                    alert={this.showAlert}
                />,
                <Button
                    variant={"outlined"}
                    onClick={() => this.deleteItem(item._id)}
                >Delete</Button>
            ]
        });

        return (
            <div className="App">
                    <Alert alert={alert}/>
                    <AddNewEmployee
                        refreshData={this._refreshItems}
                        alert={this.showAlert}
                    />
                    {
                        data.length ? (<MUIDataTable
                            title={"List of Items"}
                            data={data}
                            columns={columns}
                            options={options}
                        />) : null
                    }
            </div>
        );
    }
}
export default Employees;