import React, { Component } from 'react';
import './App.css';
import { Provider } from "react-redux";
import store from "./store";
import AppNavBar from "./components/navigations/AppNavBar";
import Item from "./components/leksioni10/Item";
import Redirect, { Router, BrowserRouter, Switch, Route } from "react-router-dom";
import Employee from "./components/leksioni12/Employee";
import Employees from "./components/detyra-12/Employees";
import Login from "./components/authentication/Login";
import Register from "./components/authentication/Register";
import { createBrowserHistory } from "history";
import PublicLayout from "./components/layouts/PublicLayout";
import PrivateLayout from "./components/layouts/PrivateLayout";
import { toast } from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';

const history = createBrowserHistory();

toast.configure();

class App extends Component {

    render() {

        return (
            <Router history={history}>
                <Provider store={store}>
                    <div className="App">
                        <BrowserRouter basename={'ReactCourse'}>
                           <Switch>
                                <Route path="/app" component={PublicLayout} />
                                <Route path="/" component={PrivateLayout} />
                           </Switch>     
                        </BrowserRouter>
                    </div>
                </Provider>
            </Router>
        );
    }
}
export default App;