import {applyMiddleware, createStore, compose} from "redux";
import itemReducer from "./redux/reducers/itemReducer";
import thunk from "redux-thunk";
import rootReducer from "./redux/reducers"

const middleware = [thunk];

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose();

const initialState = {};

const store = createStore(rootReducer, composeEnhancers(applyMiddleware(...middleware)));

export default store;
