import React, { useState } from 'react';
import PropTypes from 'prop-types';

function DisplayChildren (props) {

    const [name, setName] = useState("my name");


    return (
        <div>
            { props.myArray.map( item =>
                <h2>{item.itemName}</h2>
                ) }
        </div>
    );

}

DisplayChildren.propTypes = {
    childName: PropTypes.string.isRequired,
    handleStateChange: PropTypes.func.isRequired,
    myArray: PropTypes.array
}

DisplayChildren.defaultProps = {
    childName: "Empty",
    handleStateChange: () => {},
    myArray: [{ itemName: 'item1' }]
}

export default DisplayChildren;