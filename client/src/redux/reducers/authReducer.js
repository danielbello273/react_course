import {
    USER_LOADED,
    USER_LOADING,
    AUTH_ERROR,
    LOGIN_SUCCESS,
    LOGIN_FAIL,
    LOGOUT_SUCCESS,
    REGISTER_SUCCESS,
    REGISTER_FAIL
} from "../actions/types";

const initialState = {
    token: localStorage.getItem("token"),
    isAuthenticated: false,
    isLoading: false,
    user: null
}

const authReducer = (state=initialState, action) => {
    switch (action.type) {
        case USER_LOADING:
            return {
                ...state,
                token: localStorage.getItem('token'),
                isLoading: true
            }
        case USER_LOADED:
            return {
                ...state,
                isLoading: false,
                user: action.payload,
                isAuthenticated: true
            }
        case LOGIN_SUCCESS:
        case REGISTER_SUCCESS:
            localStorage.setItem("token", action.payload.token);
            return {

                //Since we are also getting the token, we send the whole payload
                ...state,
                ...action.payload,
                isAuthenticated: true,
                isLoading: false
            };
        case AUTH_ERROR:
        case LOGIN_FAIL:
        case LOGOUT_SUCCESS:
        case REGISTER_FAIL:
            localStorage.removeItem('token');
            return {
                ...state,
                token: null,
                isAuthenticated: false,
                user: null,
                isLoading: false
            }                
        default:
            return state;
    }
}

export default authReducer;