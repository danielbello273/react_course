import { GET_ITEMS, GET_ITEM_BY_ID, ADD_ITEM, DELETE_ITEM, UPDATE_ITEM, SET_ITEMS_LOADING } from "../actions/types";

const initialState = {
    items: [],
    loading: false
}

const itemReducer = (state=initialState, action) => {
    switch (action.type) {
        case GET_ITEMS:
            return {
                ...state,
                items: action.payload,
                loading: false
            }
        case ADD_ITEM:
            return {
                ...state,
                items: [action.payload, ...state.items]
            }
        case DELETE_ITEM:
            return {
                ...state,
                items: state.items.filter(item => item._id !== action.payload)
            }
        case GET_ITEM_BY_ID:
            return {
                ...state,
                item: action.payload
            }
        default:
            return state;
    }
}

export default itemReducer;