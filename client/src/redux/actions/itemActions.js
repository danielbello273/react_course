import {GET_ITEMS, GET_ITEM_BY_ID, ADD_ITEM, DELETE_ITEM, UPDATE_ITEM, SET_ITEMS_LOADING, AUTH_USER} from "./types";
import axios from "axios";
import { alertActions } from "./alertActions";

export const getItems = () => dispatch => {
    dispatch(setItemsLoading());
    axios.get('/api/items')
        .then(res => {
            dispatch({
                type: GET_ITEMS,
                payload: res.data
            });
        })
        .catch(err => console.log(err));
}

export const addItem = itemData => dispatch => {

    axios.post('/api/items', itemData)
        .then(res => {
            dispatch({
                type: ADD_ITEM,
                payload: res.data
            });
            dispatch(alertActions.success('Item added successfully'));
        })
        .catch(err => console.log(err));
}

export const deleteItem = itemId => dispatch => {

    axios.delete(`/api/items/${itemId}`)
        .then(res => {
            dispatch({
                type: DELETE_ITEM,
                payload: itemId
            });
        })
        .catch(err => console.log(err));
}

export const getItemByID = itemId => dispatch => {
    return axios.get(`/api/items/${itemId}`)
        .then(res => (
            dispatch({
                type: GET_ITEM_BY_ID,
                payload: res.data
            })
            )
        )
        .catch(err => console.log(err));
}

export const updateItem = itemId => dispatch => {

    axios.put(`/api/items/${itemId}`)
        .then(res => {
            dispatch({
                type: UPDATE_ITEM,
                payload: res.data
            });
        })
        .catch(err => console.log(err));
}

export const auth = user => dispatch => {

    axios.post('/api/auth', user)
        .then(res => {
            dispatch({
                type: AUTH_USER,
                payload: res.data
            });
        })
        .catch(err => console.log(err));
}

export const setItemsLoading = () => {
    return {
        type: SET_ITEMS_LOADING
    }
}

