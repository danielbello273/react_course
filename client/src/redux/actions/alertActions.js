import { toast } from "react-toastify";
import { alertConstants } from "./types";

const success = (message) => {
    toast.success(message, { position: toast.POSITION.TOP_LEFT });
    return { type: alertConstants.State.SUCCESS, message };
}

const error = (message) => {
    toast.error(message, { position: toast.POSITION.BOTTOM_RIGHT });
    return { type: alertConstants.State.ERROR, message }
}

const clear = () => {
    return { type: alertConstants.State.CLEAR }
}

export const alertActions = {
    success,
    error,
    clear
}