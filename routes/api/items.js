const express = require('express');
const router = express.Router();

//Item Model
const Item = require('../../models/item');
const cors = require('cors');


let corsOptions = {
   origin: 'http://localhost:3000',
   optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
}

//Routes

//@route get api item
//@desc get all items
//@access public
router.get('/',cors(corsOptions),(req ,res ) => {
   Item
       .find({})
       .select('name surname')
       .sort({ date: -1 }) //sort descending
       .then( items => res.json(items) )
       .catch(err => console.log(err));
});

//@route post api/item
//@desc add an item
//@access public
router.post('/', (req, res) => {
   const newItem = new Item({
      name: req.body.name,
      surname: req.body.surname,
       birthday: req.body.birthday,
       address: req.body.address,
       socialSecNr: req.body.socialSecNr,
       age: req.body.age
   });

   newItem.save().then(item => res.json(item));
});


router.delete('/:id', (req, res) => {
   Item.findById(req.params.id)
       .then(item => item.remove().then(() => res.json({success: true})))
       .catch(err => res.status(404).json({success: false}))
});

router.get('/:id', (req,res) => {
   Item.findById(req.params.id)
       .then(item => res.json(item))
       .catch(err => console.log(err));
});

// Update an item
router.put('/:id', (req,res) => {
   Item.findByIdAndUpdate(
       req.params.id, {
          name: req.body.name,
          surname: req.body.surname,
           birthday: req.body.birthday,
           address: req.body.address,
           socialSecNr: req.body.socialSecNr,
           age: req.body.age
       }
   )
       .then( item => res.json(item) )
       .catch(err => console.log(err));
});

module.exports = router;